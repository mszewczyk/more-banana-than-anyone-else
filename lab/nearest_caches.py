# -*- encoding: utf-8 -*-

import requests

urls = {
    'GoogleAddress': 'http://maps.googleapis.com/maps/api/geocode/json', 
    'OKAPI': 'http://opencaching.pl/okapi/services/caches/search/nearest?consumer_key=DFcXAdkqLcUrtQLyWmHY',
    'OKAPI_CACHE': 'http://opencaching.pl/okapi/services/caches/geocache?consumer_key=DFcXAdkqLcUrtQLyWmHY'}

#address = raw_input('Podaj adres: ')
addr='Ciolkosza 2a, Warszawa' #polskie znaki psuja, a nie ma czasu na parsowanie
query={'address':addr, 'sensor': 'false'}
# Znalezienie współrzędnych adresu
response=requests.get(urls['GoogleAddress'], params=query)
latlong=response.json()['results'][0]['geometry']['location']
lat=latlong['lat']
long=latlong['lng']
# Znalezienie najbliższych geoskrytek
query={'center':str(lat)+"|"+str(long)}
okapiresult=requests.get(urls['OKAPI'], params=query) #cache type cods
i=0;
addresses=[]
lats=[]
longs=[]
all=[]
for cache in okapiresult.json()['results']: #dla kazdego cachetypecode, wez geolokacje i wrzuc do googla i wyciagnij potrzebne info
    query = {'cache_code': cache}
    result=requests.get(urls['OKAPI_CACHE'], params=query)
    lat = result.json()['location'].split("|")[0] #wyjmuje geolokacje
    long = result.json()['location'].split("|")[1]
    # Wydobycie informacji o lokalizacji skrytek
    query = {'latlng': lat+","+long, 'sensor': 'false'}
    result=requests.get(urls['GoogleAddress'], params=query)
    parsed=result.json()['results']
    #print parsed[i]['formatted_address']
    #print parsed[i]['geometry']['location']['lat']
    #print parsed[i]['geometry']['location']['lng']
    all+=[(parsed[0]['formatted_address'],parsed[0]['geometry']['location']['lat'],parsed[0]['geometry']['location']['lng'])]
    i+=1
from operator import itemgetter #http://wiki.python.org/moin/HowTo/Sorting/
sorted(all, key=itemgetter(1,2))
print "Dostepne najblizsze skrytki, posorotowane po geolokalizacji:"
for cache in all:
    print cache
# Przydzielenie adresu skrytkom na podstawie ich współrzędnych

# Wyświetlenie wyniku
