# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup
import sys

url = {
    'login': 'https://e.sggw.waw.pl/login/index.php',
    'grades': 'http://e.sggw.waw.pl/grade/report/user/index.php?id=649'
}

username = raw_input('Użytkownik: ')
password = getpass.getpass('Hasło: ')

payload = {
    'username': username,
    'password': password
}

# Logowanie
session=requests.session()
session.post(url['login'],data=payload,verify=False)
# Pobranie strony z ocenami
response=session.get(url['grades'], verify=False)
# Odczyt strony
parsed=BeautifulSoup(response.text)
# Parsowanie strony
grades=parsed.find('table','user-grade')
if grades==None:
    print "Błąd logowania."
    sys.exit(1)
# Scraping
List={}
for tr in grades.find_all('tr')[2:]:

    tds=tr.find_all('td','b1b') #wszystko, co związane z ocenami bądź kategoriami ma w klasie b1b
    if tds==[]:
        continue

    if 'b1t' in tds[0]['class']: #kategorie dodatkowo mają b1t
        List[tds[0].text]=[]
        key=tds[0].text
    else: 
        if tds[1].text=='-':
            ocena=0.0
        else:
            ocena=float(tds[1].text.replace(',','.'))
        List[key]=List[key]+[[tds[0].text, tds[0].a['href'], ocena]]

# Sortowanie
for cathegory in List:
    List[cathegory]=sorted(List[cathegory], key=lambda element: element[2])
    List[cathegory].reverse()
# Wyświetlenie posortowanych ocen w kategoriach
for cathegory in List:
    print cathegory
    for element in List[cathegory]:
        print element[0]+": "+element[1]+" - "+str(element[2])
