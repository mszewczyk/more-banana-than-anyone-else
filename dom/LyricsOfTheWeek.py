
import webbrowser
import requests
from bs4 import BeautifulSoup
from requests.exceptions import ConnectionError

urls={
    "Lastfm":"http://ws.audioscrobbler.com/2.0",
    "ChartlyricsSearch":"http://api.chartlyrics.com/apiv1.asmx/SearchLyric",
    "ChartlyricsGet":"http://api.chartlyrics.com/apiv1.asmx/GetLyric",
    "SongsterrSongs":"http://www.songsterr.com/a/ra/songs.xml",
    "SongsterrSong":"http://www.songsterr.com/a/wa/song"
    }

LastfmAPIKey="3c16bcc9b8541000a3db445174dbcbe9"

def get_track_of_the_week(username):
    lastfm_query={"user":username, "api_key":LastfmAPIKey, "format":"json", "method":"user.getWeeklyTrackChart"}
    WeeklyTrackChart=requests.get(urls['Lastfm'],params=lastfm_query)

    try:
        WeeklyTrackChart.json()['error']
        print "No such Lastfm user!"
        quit()
    except KeyError:
        pass
    
    try:
        Track=WeeklyTrackChart.json()['weeklytrackchart']['track']['rank'==1]
    except KeyError:
        print "No track of the week!"
        quit()

    Artist=Track['artist']['#text']
    Title=Track['name']
    return Artist,Title

def get_lyrics(Artist, Title):
    lyrics_query={"artist":Artist, "song":Title}
    
    for x in range(3,0,-1):
        try:
            lyricsSearch=BeautifulSoup(requests.get(urls['ChartlyricsSearch'],params=lyrics_query).text)
            break
        except ConnectionError:
            if x==1:
                print "ChartLyrics timeout."
                return ""               
            pass

    lyricid="0"
    lyricchecksum=""
    lyricurl=""
    for lyricsResult in lyricsSearch.find_all('searchlyricresult'):
        artist=lyricsResult.find('artist')
        song=lyricsResult.find('song')
        if artist==None or song==None:
            continue
        if artist.text.lower()==Artist.lower() and song.text.lower()==Title.lower():
            lyricid=lyricsResult.find('lyricid').text
            if lyricid=="0":
                print "Couldn't find lyrics in database. Try later or help improving http://www.chartlyrics.com/"
                return ""
            lyricchecksum=lyricsResult.find('lyricchecksum').text
            lyricurl=lyricsResult.find('songurl').text
            print "Found lyrics: "+lyricurl
            break

    if lyricid=="0":
        print "Couldn't find lyrics in database. Try later or help improving http://www.chartlyrics.com/"
        return ""

    for i in range(10,0,-1):
        try:
            resp=requests.get(urls['ChartlyricsGet'], params={"lyricid":lyricid, "lyricchecksum":lyricchecksum})
            print resp
            lyrics=BeautifulSoup(resp.text)
            return lyrics.find('lyric').text
        except ConnectionError:
            if i==1:
                print "ChartLyrics timeout. Follow link above."
                return ""
            pass

def get_TabulatureLink(Artist, Title):
    try:
        resp=requests.get(urls['SongsterrSongs'], params={"pattern": Artist+" "+Title})
        try:
            song=BeautifulSoup(resp.text).find("song")
            id=song['id']
        except TypeError:
            print "There is no tabulature for this song in SongsterrSong database."
            return -1
            pass
        resp=requests.get(urls['SongsterrSong'], params={"id": id})
        print 'Found Tabulature:'
        return resp.url
    except ConnectionError:
        print "Timeout or end of the world. Dunno."
        pass
print('Showing your song of the week with lyrics and tabulature. Basing on last.fm API, Chartlyrics API and SongsterrSong API.')
print("----")
username = raw_input('last.fm username: ')  #i.e. fsfwarrior

print("----")
Artist, Title=get_track_of_the_week(username)
print("----")
print "Song of the week: "+Artist+": "+Title

Lyrics=get_lyrics(Artist, Title)
print Lyrics
print("----")
url= get_TabulatureLink(Artist, Title)
if url == -1:
    quit()
print url
browser = raw_input('Would you like to open this link in your default browser? (y for yes, rest for no) ')
if browser == 'y':
    webbrowser.open(url)
quit()
